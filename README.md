# Eureka Discovery Microservice
Este microservicio es para el registro de microservicios implementado con Eureka.

## Configuración
La configuración está pensada para desplegar este microservicio en un ambiente de QA o Producción, ya que
depende del microservicio Config Server.

Las variables de entorno que utiliza este proyecto se encuentran declaradas en el archivo `.env.example`.
Para correrlo localmente, debes realizar una copia de esta y colocarlo como `.env`.

```bash
cp .env.example .env
```

Si estás corriendo el microservicio Config Server localmente, debes colocar tus credenciales en los campos

```dotenv
CONFIG_MICROSERVICE_URL=http://127.0.0.1:8081       # Ask to support (default is for development)
SPRING_CLOUD_CONFIG_USERNAME=
SPRING_CLOUD_CONFIG_PASSWORD=
```

De lo contrario, solicitar los valores de los campos

```dotenv
CONFIG_MICROSERVICE_URL=                            # Ask to support (default is for development)
SPRING_CLOUD_CONFIG_USERNAME=                       # Ask to support
SPRING_CLOUD_CONFIG_PASSWORD=                       # Ask to support
```
